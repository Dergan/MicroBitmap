﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MicroBitmap.Compressors;
using System.Diagnostics;

namespace TestProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Filter = "Images|*.jpg;*.png;*.bmp";
                dialog.Multiselect = false;
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    MicroBitmap.MicroBitmap c = null;

                    if (radioButton1.Checked)
                        c = new MicroBitmap.MicroBitmap(new QuickCompress());

                    Bitmap tmp = new Bitmap(dialog.FileName);
                    pictureBox1.BackgroundImage = tmp;
                    label7.Text = "Image Width: " + tmp.Width;
                    label8.Text = "Image Height: " + tmp.Height;

                    Stopwatch sw = Stopwatch.StartNew();
                    byte[] compressed = c.Compress(dialog.FileName);
                    sw.Stop();
                    label3.Text = "Compress Time: " + sw.Elapsed.ToString();
                    label5.Text = "Compressed Size: " + compressed.Length;

                    long DecompressedSize = 0;
                    sw = Stopwatch.StartNew();
                    Bitmap decompressed = c.Decompress(compressed, out DecompressedSize);
                    pictureBox2.BackgroundImage = decompressed;
                    sw.Stop();
                    label4.Text = "Decompress Time: " + sw.Elapsed.ToString();
                    label6.Text = "Decompressed Size: " + DecompressedSize;
                }
            }
        }
    }
}
