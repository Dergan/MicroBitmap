﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing;

namespace MicroBitmap.Compressors
{
    public class QuickCompress : ICompressor
    {
        public PixelFormat[] SupportedFormats
        {
            get
            {
                return new PixelFormat[]
                {
                    PixelFormat.Format32bppRgb
                };
            }
        }

        public byte[] Compress(string FilePath)
        {
            return Compress((Bitmap)Bitmap.FromFile(FilePath));
        }

        public unsafe byte[] Compress(Bitmap bitmap)
        {
            List<byte> payload = new List<byte>();
            payload.AddRange(BitConverter.GetBytes(bitmap.Width));
            payload.AddRange(BitConverter.GetBytes(bitmap.Height));
            payload.AddRange(BitConverter.GetBytes((int)bitmap.PixelFormat));

            BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, bitmap.PixelFormat);
            byte* point = (byte*)data.Scan0.ToPointer();
            Pixel pixel = new Pixel(bitmap.PixelFormat, point);

            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    //pixel.NextPixel();
                }
            }

            bitmap.UnlockBits(data);
            return payload.ToArray();
        }

        public Bitmap Decompress(byte[] rawData, out long Size)
        {
            int width = BitConverter.ToInt32(rawData, 0);
            int height = BitConverter.ToInt32(rawData, 4);
            PixelFormat format = (PixelFormat)BitConverter.ToInt32(rawData, 8);
            Bitmap bmp = new Bitmap(width, height, format);

            Size = 0;
            return bmp;
        }
    }
}