﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Imaging;

namespace MicroBitmap
{
    public unsafe class Pixel
    {
        public PixelFormat pixelFormat;
        public byte R;
        public byte G;
        public byte B;
        public byte A;
        private byte* point;

        public Pixel(byte R, byte G, byte B, byte A)
        {
            this.R = R;
            this.G = G;
            this.B = B;
            this.A = A;
        }
        public Pixel(PixelFormat pixelFormat, byte* point)
        {
            this.pixelFormat = pixelFormat;
            this.point = point;

            switch (pixelFormat)
            {
                case PixelFormat.Format16bppArgb1555:
                case PixelFormat.Format32bppArgb:
                case PixelFormat.Format64bppArgb:
                {
                    this.point -= 4;
                    break;
                }
                case PixelFormat.Format16bppRgb555:
                case PixelFormat.Format16bppRgb565:
                case PixelFormat.Format24bppRgb:
                case PixelFormat.Format32bppRgb:
                case PixelFormat.Format48bppRgb:
                case PixelFormat.Format4bppIndexed:
                case PixelFormat.Format8bppIndexed:
                {
                    this.point -= 3;
                    break;
                }
            }

            NextPixel();
        }

        public void NextPixel()
        {
            switch (pixelFormat)
            {
                case PixelFormat.Format16bppArgb1555:
                case PixelFormat.Format32bppArgb:
                case PixelFormat.Format64bppArgb:
                {
                    this.point += 4;
                    this.R = point[3];
                    this.G = point[2];
                    this.B = point[1];
                    this.A = point[0];
                    break;
                }
                case PixelFormat.Format16bppRgb555:
                case PixelFormat.Format16bppRgb565:
                case PixelFormat.Format24bppRgb:
                case PixelFormat.Format32bppRgb:
                case PixelFormat.Format48bppRgb:
                case PixelFormat.Format4bppIndexed:
                case PixelFormat.Format8bppIndexed:
                {
                    this.point += 3;
                    this.R = point[2];
                    this.G = point[1];
                    this.B = point[0];
                    this.A = 255;
                    break;
                }
            }
        }

        public byte[] ToArray()
        {
            switch (pixelFormat)
            {
                case PixelFormat.Format16bppArgb1555:
                case PixelFormat.Format32bppArgb:
                case PixelFormat.Format64bppArgb:
                {
                    return new byte[] { B, G, R, A };
                }
                case PixelFormat.Format16bppRgb555:
                case PixelFormat.Format16bppRgb565:
                case PixelFormat.Format24bppRgb:
                case PixelFormat.Format32bppRgb:
                case PixelFormat.Format48bppRgb:
                case PixelFormat.Format4bppIndexed:
                case PixelFormat.Format8bppIndexed:
                {
                    return new byte[] { B, G, R };
                }
            }
            return new byte[0];
        }

        public static bool operator !=(Pixel one, Pixel two)
        {
            return (one.A != two.A || one.B != two.B || one.G != two.G || one.R != two.R) ? true : false;
        }
        public static bool operator ==(Pixel one, Pixel two)
        {
            return (one.A == two.A && one.B == two.B && one.G == two.G && one.R == two.R);
        }
    }
}