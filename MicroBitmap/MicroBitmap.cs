﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace MicroBitmap
{
    public class MicroBitmap
    {
        private ICompressor compressor;

        public MicroBitmap(ICompressor compressor)
        {
            if (compressor == null)
                throw new ArgumentNullException("compressor");
            this.compressor = compressor;
        }

        public byte[] Compress(Bitmap bitmap)
        {
            if (compressor == null)
                throw new ArgumentNullException("bitmap");

            bool supported = false;

            for (int i = 0; i < compressor.SupportedFormats.Length; i++)
            {
                if (compressor.SupportedFormats[i] == bitmap.PixelFormat)
                {
                    supported = true;
                    break;
                }
            }
            if (!supported)
                throw new NotSupportedException("This image format is not supported with this image compressor");

            return compressor.Compress(bitmap);
        }

        public byte[] Compress(string FilePath)
        {
            return compressor.Compress(FilePath);
        }

        public Bitmap Decompress(byte[] rawData, out long Size)
        {
            return compressor.Decompress(rawData, out Size);
        }
    }
}