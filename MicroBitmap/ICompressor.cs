﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace MicroBitmap
{
    public interface ICompressor
    {
        PixelFormat[] SupportedFormats { get; }
        byte[] Compress(Bitmap bitmap);
        byte[] Compress(string FilePath);
        Bitmap Decompress(byte[] rawData, out long Size);
    }
}